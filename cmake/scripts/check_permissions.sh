#!/bin/sh

permfile="permissions.log"
if [ -e $permfile ]; then
    rm -f $permfile
fi

# some packages like MCGenerators are one level lower
if (ls -d $1/*/*/$2 || ls -d $1/*/*/*/$2)  > /dev/null 2> /dev/null; then
    echo "Found directories, continuing"
else
    echo "ERROR: No directories with $1/*/*/$2 or $1/*/*/*$2"
    exit 1
fi

# some packages like MCGenerators are one level lower
find -L $1/*/*/$2 $1/*/*/*/$2 ! -perm -o=r ! -name auth_pam_tool_dir ! -name trim.txt 2> /dev/null > $permfile
SIZE=$(cat $permfile | wc -l) 
if [ $SIZE  -gt 0 ]; then
  echo "Wrong permissions:"
  cat $permfile | xargs -n 1 ls -ld
  exit 1
else
  echo "All permissions are OK."
fi
exit 0
