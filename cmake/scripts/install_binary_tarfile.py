#!/usr/bin/env python3
"""
Program to install locally the LCG binary tarfile. It downsloads, expands and relocates the binary tarfile. 
<pere.mato@cern.ch>
Version 1.0
"""

#-------------------------------------------------------------------------------
from __future__ import print_function
import os, sys, tarfile, subprocess, shutil
import platform as platform_mod
import time
import argparse
import errno

if sys.version_info[0] >= 3 :
  import urllib.request as urllib2
else:
  import urllib2

def tarFileFilterAgs():
  """Return a dictionary of filter_arguments for TarFile.

  This depends on the OS and python version, some cases security features are backported that cause warnings in our builds...

  * 3.8.10 is ubuntu20
  """
  if sys.version_info < (3,6,8) or platform_mod.system() == 'Darwin' or sys.version_info[:3] == (3,8,10):
    return {}
  # for alma8, alma9
  return dict(filter='fully_trusted')

#---install_tarfile-------------------------------------------------------------
def install_tarfile(urltarfile, prefix, lcgprefix, with_hash=True, with_link=True):
  # Check if the script got triggered via the lcg_latest_pipeline job
  latest_installation = int(os.environ.get('LATEST_INSTALLATION', 0))
  #---Installing with hash implies to install at the prefix parent directory
  #   and make a symlink to it at prefix----------------------------------------
  orig_prefix = prefix
  if with_hash and with_link :
    prefix = os.path.dirname(prefix)
  #---Download and expand tarfile-----------------------------------------------  
  try :
    filename = os.path.basename(urltarfile)
    items = os.path.splitext(filename)[0].split('-')
    hash = items[-5].split('_')[-1]
    platform = '-'.join(items[-4:])
  except:
    print("Binary tarfile name '%s' ill-formed" % filename)
    sys.exit(1)

  print('==== Downloading and installing %s' % urltarfile)
  counter = 0
  success = False
  while counter < 5 and not success:
    if counter > 0:
      sleeptime = 20 * counter
      print("Warning: Failed downloading %s, sleeping %s seconds before retry..." % (urltarfile, sleeptime))
      time.sleep(sleeptime)
    counter += 1
    dirname = version = ''
    try:
      with urllib2.urlopen(urltarfile) as resp:
        tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
        dirname, version = os.path.split(tar.next().name)
        tar.extractall(path=prefix, **tarFileFilterAgs())
        success = True
    except (urllib2.HTTPError, urllib2.URLError) as detail:
      print('Failed downloading %s : %s' % (urltarfile, detail))
    except (tarfile.ReadError, tarfile.StreamError) as detail:
      print('Failed untaring %s : %s' % (urltarfile, detail))
    except OSError as detail:
      if detail.errno == errno.ENOSPC and dirname and version and latest_installation:
        # if "no space left on device" error occurs during extraction of the tarball
        # all the already extracted files are removed again and we exit the script
        print('Insufficient disk space for %s ... removing the extracted files again' % (urltarfile))
        shutil.rmtree(os.path.join(prefix, dirname, version, platform))
        # exit without error to stop the extraction of more tarballs and trigger the publication
        sys.exit(0)
      else:
        print('Failed untaring %s : %s' % (urltarfile, detail)) 
    except:
      print('Unexpected error:', sys.exc_info()[0])
  if not success:
    print("Error: Completely failed to download file:", urltarfile)
    sys.exit(1)

  if latest_installation:
    total, used, free = shutil.disk_usage('/cvmfs/sft-nightlies.cern.ch')
    
    print(f"==== Total: {total / (2**20):.2f} MB; Used: {used / (2**20):.2f} MB; Free: {free / (2**20):.2f} MB")

    #---if less than 5% of the disk is available (quite arbitrary number, but 2000 MB out of 80000 MB on lxcvmfs171 was insufficient), exit without error
    threshold = total * 0.05
    if free < threshold:
      print(f"==== Less than 5% ({threshold / 2**20} MB) available, removing the previously installed package again to leave room for cvmfs stuff")
      shutil.rmtree(os.path.join(prefix, dirname, version, platform))
      print(f"==== After removing the previously installed package, there is {shutil.disk_usage('/cvmfs/sft-nightlies.cern.ch').free / (2**20):.2f} MB available")
      sys.exit(0)

  #---rename the version directory----------------------------------------------
  if with_hash :
    old_dirname = os.path.join(prefix, dirname, version)
    new_dirname = os.path.join(prefix, dirname, version + '-' + hash)
    if not os.path.exists(new_dirname): os.mkdir(new_dirname)
    if os.path.exists(os.path.join(new_dirname, platform)): shutil.rmtree(os.path.join(new_dirname, platform))
    os.rename(os.path.join(old_dirname, platform), os.path.join(new_dirname, platform))
    shutil.rmtree(old_dirname)
    full_version =  version + '-' + hash
  else :
    full_version = version
  install_path = os.path.join(prefix, dirname, full_version, platform)

  #---run the post-install------------------------------------------------------
  postinstall = os.path.join(install_path, '.post-install.sh')
  if os.path.exists(postinstall) :
    #---Replace the old post-install script with new one
    with open(postinstall) as f:
      script = f.read()
      if '#!/bin/sh' in script or 'RPM_INSTALL_PREFIX' not in script or 'encoding=\'utf-8\'' not in script:
        f.close()
        shutil.copy(os.path.join(os.path.dirname(os.path.realpath(__file__)),'post-install.sh'), postinstall)
    os.environ['INSTALLDIR'] = prefix
    if lcgprefix  :
      os.environ['LCGRELEASES'] = lcgprefix.replace(';',':').replace(' ',':')
    if not with_hash :
      os.environ['NIGHTLY_MODE'] = '1'
    with open(os.devnull, 'w') as devnull:
      rc = subprocess.call([postinstall], stdout=devnull)
      if rc != 0:
        raise RuntimeError("Post-install for package {0} failed!".format(filename))

  #---run the fix-mac-relocation------------------------------------------------
  if sys.platform == 'darwin':
    from fix_mac_relocation import fix_darwin_install_name
    fix_darwin_install_name(install_path, orig_prefix)

  #---create a link to the hashed install---------------------------------------
  if with_hash and with_link :
    dest = os.path.join(orig_prefix, dirname, version)
    if not os.path.exists(dest) : os.makedirs(dest)
    os.symlink(os.path.relpath(install_path, dest), os.path.join(dest, platform))

#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments-------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--url', dest='url', help='URL of the binary tarfile', required=True)
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--lcgprefix', dest='lcgprefix', help='LCG prefix to the installation', default='', required=False)
  parser.add_argument('--nohash', dest='with_hash', action='store_false', help='Install without hash', default=not 'RELEASE_MODE' in os.environ and not 'NIGHTLY_MODE' in os.environ)
  parser.add_argument('--nolink', dest='with_link', action='store_false', help='Install without linking', default=True)
  args = parser.parse_args()
  install_tarfile(args.url, args.prefix, args.lcgprefix, args.with_hash, args.with_link)
