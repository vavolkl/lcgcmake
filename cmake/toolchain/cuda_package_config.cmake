
#---Remove unneeded packages---------------------------------------
LCG_remove_package(Geant4)
LCG_remove_package(Geant4-vecgeom-scalar)
LCG_remove_package(DD4hep)
LCG_remove_package(acts)
LCG_remove_package(Gaudi)
LCG_remove_package(Garfield++)

#---Overwrites and additional packages ----------------------------
LCG_external_package(TensorRT     10.0.1.6  cuda=12.4           ) ## cuda name just indicative of highest supported version, this is 12.3 compatible
LCG_external_package(cuda         12.3      full=12.3.2_545.23.08 )
LCG_external_package(cudnn        8.9.7.29  cuda=12              )
LCG_external_package(cupy         13.0.0    cuda=12x            )
LCG_external_package(fastrlock    0.8.2                         )
LCG_external_package(llvmmin      17.0.6                        )
LCG_external_package(mako         1.3.5                         )
LCG_external_package(py_tools     2021.2.7                      )
LCG_external_package(pycuda       2024.1                        )
LCG_external_package(pyopencl     2024.1                        )
LCG_external_package(tvm          0.16.0                        )
LCG_external_package(jax_cuda12_pjrt   0.4.28                   )
LCG_external_package(jax_cuda12_plugin 0.4.28                   )


#----Overwites for Ubuntu20---------------------------------------
if( ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20 )
  LCG_remove_package(TensorRT)
endif()
