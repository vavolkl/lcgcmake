#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT v6-32-00-patches GIT=http://root.cern.ch/git/root.git)
#LCG_AA_project(ROOT  6.32.06)

LCG_external_package(Boost             1.87.0)  

#if(${LCG_OS}${LCG_OSVERS} MATCHES centos|ubuntu|el)
#  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER 7)) OR (${LCG_COMP} MATCHES clang))
#    LCG_AA_project(Gaudi  v36r14.testtbb GIT=https://gitlab.cern.ch/dkonst/Gaudi.git)
#  endif()
#endif()

#---Apple MacOS special removals and overwrites--------------------
include(heptools-macos)

if(LCG_ARCH MATCHES "aarch64")
  include(heptools-devARM)
endif()

