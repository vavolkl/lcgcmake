#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev4)

SET(LHCB_HEPMC 2)

include(heptools-dev-base)

LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

if(LCG_ARCH MATCHES "aarch64")
  include(heptools-devARM)
endif()

SET(LHCB_JSON_FILE https://gitlab.cern.ch/lhcb-core/rpm-recipes/-/raw/master/LHCBEXTERNALS/dev4lhcb.json)

include(heptools-lhcbsetup)

# with patch for py3.11
LCG_external_package(lhapdf            6.2.3p1        ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
# for py3.11
LCG_external_package(yoda              2.0.0          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             4.0.0          ${MCGENPATH}/rivet hepmc=3 author=4.0.0)
LCG_external_package(thepeg            2.2.3          ${MCGENPATH}/thepeg hepmc=${HEPMC_VERSION})
