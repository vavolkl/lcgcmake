#!/usr/bin/env python3
import sys, glob, os, datetime, shutil, time, subprocess, buildinfo2json

# Removal of nightly builds from CVMFS or AFS
#############################################
def prune_directories(path, rootdir):
    # check if p only exist of the original path that we want to unlink or delete
    prev_p = path
    p = os.path.dirname(path)
    while p != rootdir :
        # Check if the p only consists of prev_p
        if os.listdir(p) == [os.path.basename(prev_p)]:
            prev_p = p
            p = os.path.dirname(p)
        else:
            break
    print('Pruning empty directory %s' % (prev_p) if os.path.isdir(prev_p) else "Unlinking directory %s" % (prev_p))
    if os.path.islink(prev_p):
        os.unlink(prev_p)
    else:
        shutil.rmtree(prev_p)

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()

    (options, args) = parser.parse_args()
    if len(args) not in (4, 5):
        print("Please provide a slot name and platform and day of the week and backend to clean!")
        sys.exit(-2)
    slot = args[0]
    platform = args[1]
    day_option = args[2]
    endsystem=args[3]
    # implementing usage of LCG_file to determine which files can be removed and which can be reused
    LCG_File = args[4] if len(args)==5 else None

    if day_option=="tomorrow":
        day = datetime.date.fromtimestamp(time.time()+86400).strftime('%a')
    else:
        day = day_option 


    if "afs" in endsystem:
        BASE="/afs/cern.ch/sw/lcg/app/nightlies"
    else:
        BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

    print("Cleaning nightly builds of slot '%s' for day %s" % (slot, day) + (f" using LCG file {LCG_File}" if LCG_File is not None else ""))
    
    ## figuring out which files to keep
    correct_package_versions = []
    if LCG_File is not None:
        print("Downloading LCG file %s to determine which packages to keep" % LCG_File)
        LCG_url = f"https://lcgpackages.web.cern.ch/tarFiles/nightlies/{slot}/{day}/{LCG_File}"
        p = subprocess.Popen(['curl', '-L', '-s', LCG_url], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        stdout = stdout.decode()
        stderr = stderr.decode()
        assert p.returncode == 0, f"Cannot get info from {LCG_url}"
        text = stdout.strip()
        lines = text.split('\n')
        print(f"Found {len(lines)} lines in the LCG file")
        for line in lines:
            if line[0] == '#' : continue
            d = buildinfo2json.parse(line)
            correct_package_versions.append(f"{d['DIRECTORY']}/{d['VERSION']}-{d['HASH']}")

    rootdir = os.path.join(BASE, slot, day)
    dirs = glob.glob(os.path.join(rootdir, '*/*', platform))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*', platform)))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*/*', platform)))

    print(f"Found {len(dirs)} directories to clean")
    for d in dirs:
      if not os.path.exists(d):
          if os.path.islink(d):
              print(f"{d} is a broken link. Removing it.")
              prune_directories(d, rootdir)
          else:
              print(f"Skipping {d} because it does not exist anymore")
          continue
      if os.path.islink(d):
          real_path = os.path.realpath(d)
          if any([x in real_path for x in correct_package_versions]):
              print(f"Keeping {d}")
              continue
          
          print(f"directory {d} is an outdated link. Removing it.")
          prune_directories(d, rootdir)
      else:
          # can we keep the directory if the version is correct?
          print(f"directory {d} is not a link. Removing it.")
          prune_directories(d, rootdir)
