#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

REPOSITORY='sft-nightlies.cern.ch'
weekday=`date +%a`
if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_user=cvsft-nightlies
else
  cvmfs_user=cvsft
fi
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch
if [[ "${BUILDMODE}" == "nightly" ]]; then
  # -t 60, retry for 60 seconds
  echo "cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}"
  cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
else
  cvmfs_server transaction -t 6000 sft.cern.ch
fi
retVal=\$?
echo "retVal: \$retVal"
if [[ "\$retVal" == "17" && "${BUILDMODE}" == "nightly" ]]; then
    # return value 17 is transaction in progress (EEXIST)
    echo "There is an open transaction that shouldn't exist... Forcing transaction abortion"
    set +e
    # try to abort the transaction 5 times or until it is successful
    for iterations in {1..5}
    do
      cd /home
      cvmfs_server abort -f ${REPOSITORY}
      if [ "\$?" == "0" ] || [ "\$?" == "22" ]; then
        break
      else
        echo "Error aborting transaction. Going to try again in 30 seconds..."
        sleep 30
      fi
      if [[ "\$iterations" == "5" ]]; then
        echo "There is an error aborting the transaction... "
        exit 1
      fi
    done
    set -e
    cvmfs_server transaction ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
elif [ "\$retVal" != "0" ]; then
   echo "Error starting transaction"
   if [[ "${BUILDMODE}" == "nightly" ]]; then
    # try to abort the transaction, if it doesnt work, exit
    cd /home
    cvmfs_server abort -f
    if [ "\$?" != "0" ]; then
      echo "Error aborting transaction. Trying to start one once again..."
    fi
    cvmfs_server transaction ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
    if [ "\$?" != "0" ]; then
      echo "Error starting transaction. Exiting..."
      exit 1
    fi
   else
    exit 1
   fi
fi

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo Weekday:   $weekday
echo BuildMode: $BUILDMODE
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}
export PRIORITYPACKAGES="pytimber ROOT fmt"

abort=0

if [[ "${BUILDMODE}" == "nightly" ]]; then
    export NIGHTLY_MODE=1
    $WORKSPACE/lcgcmake/jenkins/clean_nightlies.py ${LCG_VERSION} $PLATFORM $weekday cvmfs LCG_${LCG_VERSION}_${PLATFORM}.txt
    rm /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}
    rm /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}
    rm /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/LCG_externals_$PLATFORM.txt
    rm /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/LCG_generators_$PLATFORM.txt
    echo "$WORKSPACE/lcgcmake/jenkins/lcginstall.py -y -u https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_${PLATFORM}.txt -p /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/ -e cvmfs"
    $WORKSPACE/lcgcmake/jenkins/lcginstall.py -y -u https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_${PLATFORM}.txt -p /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/ -e cvmfs
    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
    else
      echo "there is an error installing the packages. Let's give it a chance though ..."
    fi

    cd  /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/
    wget "https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}"
    wget "https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}"


    $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday $PLATFORM ${LCG_VERSION} RELEASE

    cd /home
    cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday

elif [[ "${BUILDMODE}" == "release" ]]; then
    if [[ "${UPDATELINKS}" == "false" ]]; then
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -u https://lcgpackages.web.cern.ch/tarFiles/releases -a https://lcgpackages.web.cern.ch/tarFiles/layered_releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    else
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -u https://lcgpackages.web.cern.ch/tarFiles/releases -a https://lcgpackages.web.cern.ch/tarFiles/layered_releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs --update
    fi

    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
      abort=1
    else
      echo "there is an error installing the packages. Exiting ..."
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi

    cd  /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION}
    $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py . $PLATFORM ${LCG_VERSION} RELEASE
    if [ ${VIEWS_CREATION} == "true" ]; then
      test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases -p $PLATFORM -r LCG_${LCG_VERSION} -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM --conflict \${PRIORITYPACKAGES}
    fi

    if [ "\$?" == "0"  -o ${VIEWS_CREATION} == "false" ]; then
      echo "The creation of the views has worked"
      cd $HOME
      cvmfs_server publish sft.cern.ch
    else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi

elif [[ "${BUILDMODE}" == "limited" ]]; then
    if [[ "${UPDATELINKS}" == "false" ]]; then
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -o -u https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    else
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -o -u https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt --update -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    fi

    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
      abort=1
    else
      echo "there is an error installing the packages. Exiting ..."
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi

    if [ ${VIEWS_CREATION} == "true" ]; then
      test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} -p $PLATFORM -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM --conflict \${PRIORITYPACKAGES}
    fi

    if [ "\$?" == "0" -o ${VIEWS_CREATION} == "false" ]; then
      echo "The creation of the views has worked"
      cd $HOME
      cvmfs_server publish sft.cern.ch
    else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi
fi
EOF
