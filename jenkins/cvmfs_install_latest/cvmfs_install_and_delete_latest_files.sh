#!/bin/bash -x
sudo -i -u cvsft-nightlies<<EOF
shopt -s nocasematch

# export environment variables that are used in the install_and_delete_latest_files.py script
export WORKSPACE=$WORKSPACE  # bring workspace from the calling environment ???
export LATEST_INSTALLATION=1
$WORKSPACE/lcgcmake/jenkins/cvmfs_install_latest/install_and_delete_latest_files.py --prefix /cvmfs/sft-nightlies.cern.ch/lcg/latest \
                                                                                    --lcgprefix /cvmfs/sft-nightlies.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases
installSuccess=\$?
cd $HOME
# abort transaction if there is an error
if [ "\$installSuccess" != "0" ]; then
  echo "There is an error installing the packages. Exiting ..."
  cvmfs_server abort -f sft-nightlies.cern.ch/lcg/latest
  exit 1
fi

cvmfs_server publish sft-nightlies.cern.ch/lcg/latest
if [ "\$installSuccess" != "0" ]; then
  echo "There is an error publishing the packages. Exiting ..."
  cvmfs_server abort -f sft-nightlies.cern.ch/lcg/latest
  exit 1
fi

EOF
