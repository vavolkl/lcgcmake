#!/usr/bin/env python3
"""
This script uses a json file that contains a list of packages to install and paths to delete.
It installs the packages and deletes the paths in the json file.
"""

import os, sys, subprocess, shutil, logging, json
import argparse

FORMAT = '%(asctime)s %(levelname)5s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger("IBR")

from collections import namedtuple
print("debug: ", os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from cmake.scripts.install_binary_tarfile import install_tarfile

blacklist = ['ROOT']

def limit_recursion(max_depth):
  # This decorator is most likely not needed, but better safe than sorry
  def decorator(func):
    def wrapper(*args, **kwargs):
      wrapper.depth += 1
      if wrapper.depth > max_depth:
        raise RecursionError(f"Maximum recursion depth ({max_depth}) exceeded")
      result = func(*args, **kwargs)
      wrapper.depth -= 1
      return result
    wrapper.depth = 0
    return wrapper
  return decorator

@limit_recursion(5)
def start_transaction(lease_path, timeout):
  LOG.info("Starting transaction for: %s", lease_path)
  p = subprocess.run(['cvmfs_server', 'transaction', '-t', str(timeout), lease_path])
  if p.returncode == 0:
    LOG.info("Successfully started transaction for: %s", lease_path)
  else:
    abort_transaction(lease_path, timeout)

@limit_recursion(5)
def abort_transaction(lease_path, timeout):
  LOG.info("Aborting transaction for: %s", lease_path)
  p = subprocess.run(['cvmfs_server', 'abort', '-f', lease_path])
  if p.returncode == 0:
    LOG.info("Successfully aborted transaction for: %s", lease_path)
    start_transaction(lease_path, timeout)
  else:
    raise RuntimeError(f"Failed to abort transaction for: {lease_path}")


#---install_and_delete_latest_files------------------------------------------------
def install_and_delete_latest_files(prefix, lcgprefix=''):
  package = namedtuple('package', ['tarfile', 'directory', 'name', 'depends'])

  # Load the JSON file with the list of packages to delete and install
  file_path = os.path.join(os.getenv('WORKSPACE'), 'package_list_for_latest.json')
  with open(file_path, 'r') as f:
    data = json.load(f)

  # Deserialize the list of packages
  packages = [package(**pkg) for pkg in data['packages_to_install']]
  paths_to_delete = data['paths_to_delete']

  lease_path = prefix.removeprefix('/cvmfs/')
  timeout = 36000
  start_transaction(lease_path, timeout)

  #---Delete the packages now---------------------------------------------------
  LOG.info("Deleting %s paths", len(paths_to_delete))
  for index, directory in enumerate(paths_to_delete):
    LOG.info("(%s/%s) Deleting directory: %s", index, len(paths_to_delete), directory)
    shutil.rmtree(directory, ignore_errors=True)
    #---Prune empty directories-----------------------------------------------
    p = os.path.dirname(directory)
    if not os.path.exists(p):
      continue
    while p != prefix :
        if not os.listdir(p):
            LOG.info('Removing empty directory: %s', p)
            os.rmdir(p)
        elif os.listdir(p) == [".cvmfscatalog"]:
          LOG.info('Removing catalog only directory: %s', p)
          shutil.rmtree(p, ignore_errors=True)
        p = os.path.dirname(p)

  #---loop over the sorted tarfiles---------------------------------------------
  LOG.info("Installing packages now")
  for index, package in enumerate(packages, start=1):
    LOG.info("(%s/%s) Installing package: %s: url=%s", index, len(packages), package.name, package.tarfile)
    install_tarfile(urltarfile=package.tarfile, prefix=prefix, lcgprefix=lcgprefix, with_hash=True, with_link=False)

  # publishing happens inside of the install_and_delete_latest_files.sh script because if we run out of
  # disk space inside of the install_tarfile() function, we completely exit the script and publish

#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments---------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--lcgprefix', dest='lcgprefix', help='LCG prefix to the installation', default='', required=False)
  
  args = parser.parse_args()
  LOG.debug("Parsed parameters")
  LOG.debug(f"Installing repository: prefix=%s, lcgprefix=%s", args.prefix, args.lcgprefix)
  install_and_delete_latest_files(args.prefix, args.lcgprefix)
