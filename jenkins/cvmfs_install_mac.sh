#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

# setting repository for nightly builds
REPOSITORY='sft-nightlies.cern.ch'
weekday=`date +%a`
if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_user=cvsft-nightlies
else
  cvmfs_user=cvsft
fi
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch
if [[ "${BUILDMODE}" == "nightly" ]]; then
  # -t 60, retry for 60 seconds
  echo "cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}"
  cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
else
  cvmfs_server transaction -t 6000 sft.cern.ch
fi
retVal=\$?
echo "retVal: \$retVal"
if [[ "\$retVal" == "17" && "${BUILDMODE}" == "nightly" ]]; then
  # return value 17 is transaction in progress (EEXIST)
  echo "There is an open transaction that shouldn't exist... Forcing transaction abortion"
  set +e
  # try to abort the transaction 5 times or until it is successful
  for iterations in {1..5}
  do
    cd /home
    cvmfs_server abort -f ${REPOSITORY}
    if [ "\$?" == "0" ] || [ "\$?" == "22" ]; then
      break
    else
      echo "Error aborting transaction. Going to try again in 30 seconds..."
      sleep 30
    fi
    if [[ "\$iterations" == "5" ]]; then
      echo "There is an error aborting the transaction... "
      exit 1
    fi
  done
  set -e
  cvmfs_server transaction ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
elif [ "\$retVal" != "0" ]; then
  echo "Error starting transaction"
  if [[ "${BUILDMODE}" == "nightly" ]]; then
    # try to abort the transaction, if it doesnt work, exit
    cd /home
    cvmfs_server abort -f
    if [ "\$?" != "0" ]; then
      echo "Error aborting transaction. Exiting..."
      exit 1
    fi
    cvmfs_server transaction ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
    if [ "\$?" != "0" ]; then
      echo "Error starting transaction. Exiting..."
      exit 1
    fi
  else
    exit 1
  fi
fi

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_${PLATFORM}.txt
echo Weekday:   ${weekday}
echo BuildMode: ${BUILDMODE}
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

set -x

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}

if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/* /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
  if [ "\$?" != "0" ]; then
    echo "There was an error installing packages, quitting"
    cvmfs_server abort -f ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}
    exit 1
  fi
  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday} ${PLATFORM} ${LCG_VERSION} RELEASE
  cd $HOME
  cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}

elif [[ "${BUILDMODE}" == "release" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/releases /cvmfs/sft.cern.ch/lcg
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}
  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} $PLATFORM $LCG_VERSION RELEASE
  cd $HOME
  cvmfs_server publish sft.cern.ch
fi
EOF
