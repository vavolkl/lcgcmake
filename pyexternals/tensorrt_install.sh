#!/bin/bash

set -eux

TENSORRT_PROJECT_DIR=$1
TENSORRT_INSTALL_DIR=$2

# Link to header files
#
# here we go from the projects folder and create the header file link
mkdir -p ${TENSORRT_INSTALL_DIR}/include/
for FILE in ${TENSORRT_PROJECT_DIR}/include/*.h; do
    ln -v -s -f ${FILE} ${TENSORRT_INSTALL_DIR}/include/$(basename $FILE)
done

# Replace static libraries with links
#
# here we take the static archives in intall directory, because we have to extract the shared libraries anyway
for FILE in ${TENSORRT_INSTALL_DIR}/lib/*.a; do
    ln -v -s -f ${TENSORRT_PROJECT_DIR}/lib/$(basename $FILE) $FILE
done

