#!/bin/bash

PYTHON=$1
PIP_HOME=$2
PREFIX=$3
TENSOR_RT_FOLDER=$4
PYVERSION=$5

${PYTHON} ${PIP_HOME}/bin/pip install --no-deps --prefix=${PREFIX}  ${TENSOR_RT_FOLDER}/tensorrt*-${PYVERSION}-none-linux_x86_64.whl
